Vagrant.configure("2") do |config|

  config.vm.define "vagrant" do |x|
    x.vm.box = "generic/rocky8"
    x.vm.network "private_network", ip: "172.16.1.100"
    x.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/"
    x.vm.provision "shell", inline: "dnf update -y"
    x.vm.provision "shell", inline: "dnf install -y htop tree nano which git tcpdump strace"

    x.vm.provision "shell", inline: "cp -av /vagrant/provisioning/config /etc/selinux/config"
    x.vm.provision "shell", inline: "chown root:root /etc/selinux/config"

    x.vm.provision :reload

    x.vm.provision "shell", inline: "dnf install -y https://repo.opensciencegrid.org/osg/23-main/osg-23-main-el8-release-latest.rpm"
    # x.vm.provision "shell", inline: "dnf install -y xrootd xrootd-client xrootd-multiuser xrootd-scitokens python3-scitokens"
    x.vm.provision "shell", inline: "dnf install -y xrootd-1:5.6.8-1.1.osg23.el8.x86_64 xrootd-client-1:5.6.8-1.1.osg23.el8.x86_64 xrootd-multiuser xrootd-scitokens-1:5.6.8-1.1.osg23.el8.x86_64 python3-scitokens"

    x.vm.provision "shell", inline: "adduser user1"
    x.vm.provision "shell", inline: "adduser user2"
    x.vm.provision "shell", inline: "chmod o+rx /home/vagrant/"
    x.vm.provision "shell", inline: "mkdir -p /home/vagrant/xrootddata/user1"
    x.vm.provision "shell", inline: "mkdir -p /home/vagrant/xrootddata/user2"
    x.vm.provision "shell", inline: "echo 'this file belongs to user1' >/home/vagrant/xrootddata/user1/testfile1.txt"
    x.vm.provision "shell", inline: "echo 'this file belongs to user2' >/home/vagrant/xrootddata/user2/testfile2.txt"
    x.vm.provision "shell", inline: "chown -R xrootd:xrootd /home/vagrant/xrootddata"
    x.vm.provision "shell", inline: "chown -R user1:user1 /home/vagrant/xrootddata/user1"
    x.vm.provision "shell", inline: "chown -R user2:user2 /home/vagrant/xrootddata/user2"
    x.vm.provision "shell", inline: "chmod -R 701 /home/vagrant/xrootddata"

    x.vm.provision "shell", inline: "cp -av /vagrant/provisioning/xrootd-http.cfg /etc/xrootd/"
    x.vm.provision "shell", inline: "cp -av /vagrant/provisioning/scitokens.cfg /etc/xrootd/"
    x.vm.provision "shell", inline: "cp -av /vagrant/provisioning/mapfile /etc/xrootd/"
    x.vm.provision "shell", inline: "cp -av /vagrant/provisioning/xrdhttp@.socket /etc/systemd/system/"
    x.vm.provision "shell", inline: "chown root:root /etc/systemd/system/xrdhttp@.socket"

    x.vm.provision "shell", inline: "systemctl start xrdhttp@http.socket"
    x.vm.provision "shell", inline: "systemctl enable xrdhttp@http.socket"
    x.vm.provision "shell", inline: "systemctl start xrootd-privileged@http"
    x.vm.provision "shell", inline: "systemctl enable xrootd-privileged@http"

    x.vm.provision "shell", inline: "dnf install -y libcurl-devel libuuid-devel sqlite-devel libcap-devel"

  end

end

# This Vagrantfile creates an xrootd demo setup with the following features:
# - xrootd serves http requests
# - if supplied with a valid Scitoken, xrootd will perform the stat/read/write operations
#   as the user specified in /etc/xrootd/mapfile, which maps the user specified in the token
#   to UNIX usernames on the xrootd server machine.
#
# USAGE:
# Create a scitoken here: https://sciauth.org/notebook-demo
# Specify the following to create a token to read files as user1:
# payload = {
#  "sub": "demouser1",
#  "scope": "read:/"
# }
# Specify the following to create a token to read files as user2:
# payload = {
#    "sub": "demouser2",
#    "scope": "read:/"
# }
#
# Get an example curl command by running the last cell in the notebook.
# Substitute the file to be read with either
# http://127.0.0.1//home/vagrant/xrootddata/user1/testfile1.txt
# or
# http://127.0.0.1//home/vagrant/xrootddata/user2/testfile2.txt
# 
# yourmachine$ vagrant up
# yourmachine$ vagrant ssh
# [root@rocky8 vagrant]# tree -ugp
# .
# └── [drwx-----x xrootd   xrootd  ]  xrootddata
#     ├── [drwx------ user1    user1   ]  user1
#     │   └── [-rw------- user1    user1   ]  testfile1.txt
#     └── [drwx------ user2    user2   ]  user2
#         └── [-rw------- user2    user2   ]  testfile2.txt
# 
# 3 directories, 2 files

# [vagrant@rocky8 ~]$ TOKEN="Authorization: Bearer eyJhbGciOiJFUzI1NiIsImtpZCI6ImtleS1lczI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0b2tlbnVzZXIxIiwic2NvcGUiOiJyZWFkOi8iLCJ2ZXIiOiJzY2l0b2tlbjoyLjAiLCJhdWQiOiJodHRwczovL2RlbW8uc2NpdG9rZW5zLm9yZyIsImlzcyI6Imh0dHBzOi8vZGVtby5zY2l0b2tlbnMub3JnIiwiZXhwIjoxNzA1NTA1NDY4LCJpYXQiOjE3MDU1MDQ4NjgsIm5iZiI6MTcwNTUwNDg2OCwianRpIjoiN2NiMGQ5MTUtODc5Yi00OWJjLWE3MDUtY2VlMGE1YThkZmJhIn0.lOs-18jJKHP3AXUMvy5VusFcQzc8jR_-HDDHYMBKqKha9cHoV0R2fUFVYg56qVInqn8qudZVPxq2_Xjm5BTMPA"
# [vagrant@rocky8 ~]$ curl -H "$TOKEN" http://127.0.0.1//home/vagrant/xrootddata/user1/testfile1.txt
# [vagrant@rocky8 ~]$ curl -H "$TOKEN" http://127.0.0.1//home/vagrant/xrootddata/user2/testfile2.txt
# 
# Expectation: reading files of user1 must only succeed with a token of demouser1
#              reading files of user2 must only succeed with a token of demouser2
#
# Writing a file:
# [vagrant@rocky8 ~]$ curl -H "$TOKEN" -X PUT -d 'this file was uploaded using curl\n' http://127.0.0.1//home/vagrant/xrootddata/user2/curlupload.txt
# :-)[vagrant@rocky8 ~]$ 
# [root@rocky8 vagrant]# tree -ugp
# .
# └── [drwx-----x xrootd   xrootd  ]  xrootddata
#     ├── [drwx------ user1    user1   ]  user1
#     │   └── [-rw------- user1    user1   ]  testfile1.txt
#     └── [drwx------ user2    user2   ]  user2
#         ├── [-rw-rw-r-- user2    user2   ]  curlupload.txt
#         └── [-rw------- user2    user2   ]  testfile2.txt
# 
# 3 directories, 3 files


# https://github.com/opensciencegrid/xrootd-multiuser/tree/master
# https://github.com/xrootd/xrootd/tree/master/src/XrdSciTokens


# Usage with GSI AAI:
# 1. Get Token BLA from AAI.
# 2. $ TOKEN="Authorization: Bearer $BLA"
# 3. $ curl -H "$TOKEN" http://127.0.0.1//home/vagrant/xrootddata/user1/testfile1.txt