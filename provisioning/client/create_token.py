#!/usr/bin/python3

import json, requests, scitokens
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("--sub", help="specify username or otherwise unique string to identify user (default=tokenuser1)", default="tokenuser1")
ap.add_argument("--scope", help="specify permissions (default=read:/)", default="read:/")
args = ap.parse_args()

def getToken(payload: dict):
    data = json.dumps({'algorithm': "ES256", 'payload': payload})
    resp = requests.post("https://demo.scitokens.org/issue", data=data)
    return resp.text

payload = {
    "sub": args.sub,
    "scope": args.scope
}
token = getToken(payload)

print(f"Authorization: Bearer {token}")